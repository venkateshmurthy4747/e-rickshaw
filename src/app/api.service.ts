import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class APIService {

  constructor(private http:HttpClient) { }
getAllData(){
  return this.http.get('http://192.168.1.126:5000/data');
  // return this.http.get('https://jsonplaceholder.typicode.com/users');
  // return this.http.get('http://1.186.146.248:5006/data');

}
}
